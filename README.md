# README #

Implementation for Task nr. 1

### Dependencies ###

Using localstack and awscli-local, you can run this without a need for an actual AWS account. 
Simply install using pip:

```
pip3 install localstack
pip3 install awscli-local
```

And then start localstack:

```
localstack start
```

### Assumption and limitations ###

This program has the following assumptions:

* Access to AWS SQS is mocked via localstack
* The program unfortunately does not take as argument the path of the file.


### How to run from commnand line ###

1. Start localstack locally via `localstack start`
2. Setup the queue: run `python3 make_queue.py`.
3. To run the receiver from command line, call `python3 receive.py`. 
* The program assumes that there already exists a queue. 
* To stop the receiver, press CTRL+C.
4. To run the sender from command line, call `python3 send.py`. 
* The program assumes that there already exists a queue. 
* The sender will send the contents of the file once, then exit.

