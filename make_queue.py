#!/usr/bin/python3
import sys
import boto3

def getQueue(): 
    """ 
    Retrieves the SQS Queue used by both the send() and receive() functions 
    """
    session = boto3.Session(
        aws_access_key_id="foo",
        aws_secret_access_key="foo",
        aws_session_token="foo",
        region_name="test0"
    )

    sqs = session.resource('sqs', endpoint_url="http://localhost:4576")
    return sqs.get_queue_by_name(QueueName='test.fifo')


def create_queue(name):
    # Get the service resource
    session = boto3.Session(
        aws_access_key_id="foo",
        aws_secret_access_key="foo",
        aws_session_token="foo",
        region_name="test0"
    )
    sqs = session.resource('sqs', endpoint_url="http://localhost:4576")

    # Create the queue. This returns an SQS.Queue instance
    queue = sqs.create_queue(
        QueueName=name, 
        Attributes={
            'DelaySeconds': '1',
            'FifoQueue': 'true'
        })

    print('Queue URL: ', queue.url)
    
def remove_queue(name):
    # Get the service resource
    session = boto3.Session(
        aws_access_key_id="foo",
        aws_secret_access_key="foo",
        aws_session_token="foo",
        region_name="test0"
    )
    sqs = session.resource('sqs', endpoint_url="http://localhost:4576")

    # Create the queue. This returns an SQS.Queue instance
    sqs.get_queue_by_name(QueueName=name).delete()


if __name__ == '__main__':
    remove_queue("test.fifo")
    create_queue("test.fifo")
