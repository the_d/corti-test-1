import boto3
import sys
import time
import asyncio
import logging
from make_queue import getQueue

async def send(filename="res/zendocuml.txt"):
    """ 
    Send the contents of the file to the SQS Queue 
    """
    
    queue = getQueue()
    print('Queue url: ', queue.url)

    lineNumber = 0
    with open(filename, 'r') as f:
        lines = []

        for line in f:
            # uncomment below line to introduce some delay
            # await asyncio.sleep(0.1)
            lineNumber += 1

            # messages are sent in batches
            # first, create the message
            processedLine = { 'Id': filename+":"+str(lineNumber), # This should be unique. Could use a Hash with a time seed to be sure.
                'MessageBody': line,
                'MessageAttributes': {
                        'filename': {
                            'StringValue': filename,
                            'DataType': 'String'
                        },
                        'line_number': {
                            'StringValue': str(lineNumber),
                            'DataType': 'Number'
                        }    
                },
                'MessageDeduplicationId': filename+":"+str(lineNumber),

                #using the filename to ensure FIFO across batches
                'MessageGroupId': filename }

            if ready_to_batch(lines, processedLine):
                # batch limit will be reached if we add the latest processed line. 
                await write_to_sqs(queue, lines)
                lines.clear()
            else:
                # batch limit not reached. append
                lines.append(processedLine)
                
            print("Sending from: ", filename, " [ ", lineNumber, "]: ", line)
    return

def ready_to_batch(messages, newMessage):
    """
        Returns true if the messages are ready to be sent as a batch message.
        This is true when either we have at least 10 messages, or the total payload is over 256 KB.
    """
    return len(messages) >= 10 or sum(len(i) for i in messages) + len(newMessage) >= 256 * 1024;    

async def write_to_sqs(queue, lines):
    """ 
    Writes batch message to the the SQS Queue 
    """

    # TODO (dota) -- should do processing and check that message was actually sent 
    return queue.send_messages(
        Entries = lines
        )

if __name__=='__main__':
    """ 
    Simulated having the sending and receiving of messages on different processes
    """
    event_loop = asyncio.get_event_loop()
    
    try:
        event_loop.run_until_complete(send())
    finally:
        event_loop.close()


    