import boto3
import sys
import time
import sys
import asyncio
from make_queue import getQueue

continue_receiving = True
# event to signal when the receive loop is done
received_done = asyncio.Event()

async def receive():
    """ 
    Receive messages from the SQS Queue. 
    This should be called multiple times to poll the Queue for new messages
    """
    queue = getQueue() # get the queue
    
    # read all messages from the queue.
    while continue_receiving is True:
        #reload queue attributes.
        queue.reload()
        if (int(queue.attributes['ApproximateNumberOfMessages']) <= 0):
            await asyncio.sleep(1)
            continue
    
        # Process messages by printing out body
        messages = queue.receive_messages(MaxNumberOfMessages=10, AttributeNames=['SentTimestamp'],MessageAttributeNames=['All'], WaitTimeSeconds=1)
        
        # sort the messages, as they are not sorted in a batch
        messages.sort(key=lambda message: int(message.message_attributes.get('line_number').get('StringValue')))
        
        for message in messages:
            filename = None
            lineNumber = -1

            # get the filename and line number, if they were set
            if message.message_attributes is not None:
                filename = message.message_attributes.get('filename').get('StringValue') + ".out"
                lineNumber = message.message_attributes.get('line_number').get('StringValue')

            if filename is not None and lineNumber is not None:
                print("Received from: " + filename + " [" + lineNumber + "]: " + message.body)
                await write_to_file(message.body, filename, lineNumber )
            else:
                print("Received from: ? : " + message.body)
                await write_to_file(message.body)
            
            # Let the queue know that the message is processed
            # This can be optimized by doing batch deletes
            message.delete()

    received_done.set()
    return

async def write_to_file(message, filename='out/queue.log', lineNumber=""):
    """ 
    Write the message to the file. 
    """
    # this is inefficient, to keep opening and closing the file for each line
    with open(filename, 'a') as f:
        f.write("[" + lineNumber +"]: " + message)

if __name__=='__main__':
    """ 
    Simulated having the sending and receiving of messages on different processes
    """
    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(receive())
        event_loop.run_forever()   
    except KeyboardInterrupt:
        pass
    finally:
        continue_receiving = False
        received_done.clear()
        event_loop.run_until_complete(received_done.wait())
        event_loop.close()
